<?php

namespace Drupal\custom_4xx_pages\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Custom 4xx Configuration Item entities.
 */
interface Custom4xxConfigEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
